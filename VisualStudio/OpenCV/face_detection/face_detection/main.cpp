#include <opencv2\opencv.hpp>
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <ctime>
#include <iostream>
#include <stdio.h>
#include <list>
using namespace std;
using namespace cv;


//HAAR classifier more accurate but slower than LBP classifier

/** Function Headers */
void detectAndDisplay(Mat frame);
double time_average(list<int> &times);

/** Global variables */
String face_cascade_name = "../data/haarcascades/haarcascade_frontalface_alt.xml";
String eyes_cascade_name = "../data/haarcascades/haarcascade_eye_tree_eyeglasses.xml";
CascadeClassifier face_cascade;
CascadeClassifier eyes_cascade;
string window_name = "Capture - Face detection";
RNG rng(12345);
string file_name = "../files/images/face_woman.jpg";
string file_name2 = "../files/images/gala.jpg";


int main(int argc, const char** argv)
{
	//CvCapture* capture;
	//Mat frame;
	Mat capture;
	VideoCapture camera(0);

	int t1, t2;
	list<int> times;

	//-- 1. Load the cascades
	if (!face_cascade.load(face_cascade_name)) { printf("--(!)Error loading\n"); return -1; };
	if (!eyes_cascade.load(eyes_cascade_name)) { printf("--(!)Error loading\n"); return -1; };

	//-- 2. Read the video stream
	//capture = cvCaptureFromCAM(-1);
	t1 = clock();
	camera >> capture;
	if(!capture.empty())
	{
		while(true)
		{
			//frame = cvQueryFrame(capture); //gives error in conversion
			/*IplImage * ipl = cvQueryFrame(capture);
			frame = cv::cvarrToMat(ipl);*/
			//-- 3. Apply the classifier to the frame
			if (!capture.empty())
			{
				detectAndDisplay(capture);
				if (times.size() >= 10)
					times.pop_back();
				times.push_back(clock()-t1);
				printf("Time to find face: %f ms\n", time_average(times) );
			}
			else
			{
				printf(" --(!) No captured frame -- Break!\n"); break;
			}

			int c = waitKey(10);
			if ((char)c == 'c')
				break;
			
			t1 = clock();
			camera >> capture;
		}
	}
	else
		printf("error in capture\n");

	//cvReleaseCapture(&capture);
	destroyAllWindows();
	camera.release();
	system("pause");
	return 0;
}

int main2(int argc, char* args[]) {

	Mat	image;
	VideoCapture camera(0);
	
	double t1, t2;
	t1 = clock();

	camera >> image;
	//image = cv::imread(file_name2, CV_LOAD_IMAGE_COLOR);
	

	if (image.empty())
		return 1;
	if (!face_cascade.load("../data/haarcascades/haarcascade_frontalface_alt.xml"))
		return 2;
	if (!eyes_cascade.load("../data/haarcascades/haarcascade_eye_tree_eyeglasses.xml"))
		return 3;
	
	resize(image, image, cv::Size(), 0.5, 0.5);
	
	detectAndDisplay(image);

	t2 = clock();
	printf("Time to find face: %lf ms", t2 - t1);

	waitKey(0);
	destroyAllWindows();
	camera.release();

	
	return 0;
}

void detectAndDisplay(Mat frame)
{
	std::vector<Rect> faces;
	Mat frame_gray;

	cvtColor(frame, frame_gray, CV_BGR2GRAY);
	equalizeHist(frame_gray, frame_gray);

	//-- Detect faces
	face_cascade.detectMultiScale(frame_gray, faces, 1.1, 2, 0 | CV_HAAR_SCALE_IMAGE, Size(30, 30));

	for (size_t i = 0; i < faces.size(); i++)
	{
		Point center(faces[i].x + faces[i].width*0.5, faces[i].y + faces[i].height*0.5);
		//ellipse(frame, center, Size(faces[i].width*0.5, faces[i].height*0.5), 0, 0, 360, Scalar(255, 0, 255), 4, 8, 0);
		cv::rectangle(frame, cv::Point(center.x + faces[i].width*0.5, center.y + faces[i].height*0.5), cv::Point(center.x - faces[i].width*0.5, center.y - faces[i].height*0.5), cv::Scalar(0,0,255), 1);

		Mat faceROI = frame_gray(faces[i]);
		std::vector<Rect> eyes;

		eyes_cascade.detectMultiScale(faceROI, eyes, 1.1, 2, 0 | CV_HAAR_SCALE_IMAGE, Size(30, 30));

		for (size_t j = 0; j < eyes.size(); j++)
		{
			Point center(faces[i].x + eyes[j].x + eyes[j].width*0.5, faces[i].y + eyes[j].y + eyes[j].height*0.5);
			int radius = cvRound((eyes[j].width + eyes[j].height)*0.25);
			circle(frame, center, radius, Scalar(255, 0, 0), 3, 8, 0);
		}
	}
	
	imshow(window_name, frame);
}

double time_average(list<int> &times) {
	list<int>::iterator it;
	int sum = 0;
	for(it=times.begin(); it!=times.end(); ++it){
		sum += *it;
	}
	return float(sum) / times.size();
}
