#define _CRT_SECURE_NO_WARNINGS

#include <fstream>
#include <iostream>
#include <time.h>
#include <string>
#include <windows.h>

using std::string;
using std::cout;
using std::endl;

string get_local_time() {
	time_t t = time(0);
	char cstr[15];
	strftime(cstr, sizeof(cstr), "%I:%M:%S%p", localtime(&t)); //complete: "%I:%M:%S %p %Z"
	return cstr;
}

int main() {

	while (true) {
		string filename = "turn_off_time.txt";
		std::ofstream fout;
		fout.open(filename.c_str(), std::fstream::out);
		fout << get_local_time();
		fout.close();
		cout << get_local_time() << endl;
		Sleep(1000);
	}

}