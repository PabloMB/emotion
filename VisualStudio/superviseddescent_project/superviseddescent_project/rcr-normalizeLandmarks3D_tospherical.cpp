
#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <stdlib.h>     /* srand, rand */
#include <cmath>

#include "utils.h"

using std::vector;
using std::string;
using std::cout;
using std::endl;

#define LANDMARKS_NUM 68
#define COORDINATES_NUM 3
#define EMOTIONS_NUM 11 //FROM 0 TO 10
#define EMOTIONS_TO_TAKE 8 //FROM 0 TO 7
#define PI   3.1415926535897932384626433832795
#define PRINT_EVERY_N 1000

void get_data_from_csv(const string filename, vector<vector<string>>& matrix) {
	std::ifstream inputfile(filename);
	string current_line;
	matrix.clear();

	while (getline(inputfile, current_line)) {
		vector<string> vect;
		std::stringstream temp(current_line);
		string single_value;
		while (getline(temp, single_value, ',')) {
			vect.push_back(single_value);
		}
		matrix.push_back(vect);
	}
};

void write_csv(const string filename, const vector<vector<string>>& matrix)
{
	std::ofstream myfile;
	myfile.open(filename.c_str());
	

	for (int i = 0; i < matrix.size(); i++) {
		if (matrix[i].size() >= LANDMARKS_NUM * 2) {

			for (int j = 0; j < matrix[i].size(); j++) {
				myfile << matrix[i][j];
				if (j != matrix[i].size() - 1)
					myfile << ",";
			}
			myfile << endl;

		}
	}

	myfile.close();
}

void normalize_spherical(vector<string>& vector)
{
	for (int j = 0; j < LANDMARKS_NUM; j++) {
		double r = stod(vector[j]);
		double t = stod(vector[j+LANDMARKS_NUM]);
		double p = stod(vector[j+LANDMARKS_NUM*2]);

		r = r/sqrt(LANDMARKS_NUM); //radius
		t = t/PI;                  //theta
		p = p/(2*PI);              //phi

		vector[j] = std::to_string(r);
		vector[j+LANDMARKS_NUM] = std::to_string(t);
		vector[j+LANDMARKS_NUM*2] = std::to_string(p);
	}
}

void normalize_spherical(vector<vector<string>>& matrix)
{
	for (int i = 0; i < matrix.size(); i++) {
		if (matrix[i].size() >= LANDMARKS_NUM * COORDINATES_NUM) {
			normalize_spherical(matrix[i]);
			if (i % PRINT_EVERY_N == 0 )
				cout << "line: " << i << endl;
		}
	}
}

void normalize_angles(vector<string>& vector)
{
	//scale all angles to be between 0 and 1 (initially they are between -2PI and 2PI )
	for (int j = 0; j < 3; j++) {
		int index = LANDMARKS_NUM * 2 + j;
		double value = stod(vector[index]);
		//cout << value << " to ";
		value = (value + PI) / (2*PI);
		//cout << value << endl;
		vector[index] = std::to_string(value);
	}

}

void normalize_angles(vector<vector<string>>& matrix)
{
	for (int i = 0; i < matrix.size(); i++) {
		if (matrix[i].size() >= LANDMARKS_NUM * 2) {
			normalize_angles(matrix[i]);
			if (i % PRINT_EVERY_N == 0)
				cout << "line: " << i << endl;
		}
	}
}


void to_spherical(vector<string>& vector) {
	for (int j = 0; j < LANDMARKS_NUM; j++) {
		//cout << "landmark: " << j << endl;
		//cout << vector[j] << endl;
		double x = stod(vector[j]);
		//cout << vector[j + LANDMARKS_NUM] << endl;
		double y = stod(vector[j+LANDMARKS_NUM]);
		//cout << vector[j + LANDMARKS_NUM*2] << endl;
		double z = stod(vector[j+LANDMARKS_NUM*2]);

		double r = sqrt(x*x + y*y + z*z);             //radius
		double t = atan2(y,x);     //theta
		double p = acos(z/r); //phi

		vector[j] = std::to_string(r);
		vector[j + LANDMARKS_NUM] = std::to_string(t);
		vector[j + LANDMARKS_NUM * 2] = std::to_string(p);
	}
}
void to_spherical(vector<vector<string>>& matrix) {
	for (int i = 0; i < matrix.size(); i++) {
		if (matrix[i].size() >= LANDMARKS_NUM * COORDINATES_NUM) {
			to_spherical(matrix[i]);
			if (i % PRINT_EVERY_N == 0)
				cout << "line: " << i << endl;
		}
	}
}

void add_ones(vector<vector<string>>& matrix)
{
	for (int i = 0; i < matrix.size(); i++) {
		if (matrix[i].size() >= LANDMARKS_NUM * 2) {
			if (i % PRINT_EVERY_N == 0)
				cout << "line: " << i << endl;
			for (int j = 0; j < 11; j++)
				matrix[i].push_back("0");
			int val = atoi(matrix[i][LANDMARKS_NUM*2+3].c_str());
			matrix[i][LANDMARKS_NUM*2+3 + 1 + val] = "1";
		}
	}
}

void delete_emotions(vector<vector<string>>& matrix_origin, vector<vector<string>>& matrix_final) {

	int ei = LANDMARKS_NUM * 3; //emotion index
	for (int i = 0; i < matrix_origin.size(); i++) {
		if (i % PRINT_EVERY_N == 0)
			cout << "line: " << i << endl;
		int val = atoi(matrix_origin[i][ei].c_str());
		if (val < EMOTIONS_TO_TAKE) { //only the first 8 emotions
			matrix_final.push_back(matrix_origin[i]);
		}
	}
}

void divide_data(vector<vector<string>>& matrix_origin, vector<vector<string>>& matrix_train, vector<vector<string>>& matrix_test, float separation) {
	int num_train = matrix_origin.size()*separation;
	int count = 0;
	srand (time(NULL));

	if (separation > 1 || separation < 0) {
		cout << "Separation must be between 0 and 1. Setting separation to 0.5" << endl;
		separation = 0.5;
	}
	for (int i = 0; i < matrix_origin.size(); i++) {
		if (i % PRINT_EVERY_N == 0)
			cout << "line: " << i << endl;

		float prob = rand() % 101 / 100;
		if (prob <= separation && count <= num_train) {
			matrix_train.push_back(matrix_origin[i]);
			count++;
		}
		else
			matrix_test.push_back(matrix_origin[i]);
	}
}

void divide_data2(vector<vector<string>>& matrix_origin, vector<vector<string>>& matrix_train, vector<vector<string>>& matrix_test, float separation) {

	srand(time(NULL));

	//count emotions of each type
	int emotions[EMOTIONS_TO_TAKE];
	int count[EMOTIONS_TO_TAKE];
	for (int i = 0; i < EMOTIONS_TO_TAKE; i++) {
		emotions[i] = 0;
		count[i] = 0;
	}
	int ei = LANDMARKS_NUM * 3; //emotion index
	int emotion;
	int total = 0;
	vector<vector<string>> matrix[EMOTIONS_TO_TAKE];
	for (int i = 0; i < matrix_origin.size(); i++) {
		emotion = atoi(matrix_origin[i][ei].c_str());
		emotions[emotion]++;
		total++;
		matrix[emotion].push_back(matrix_origin[i]);
	}
	int min = 1000000;
	for (int i = 0; i < EMOTIONS_TO_TAKE; i++) {
		cout << "emotion " << i << ": " << emotions[i] << "(" << emotions[i] * 100 / total << "%)" << endl;
		if (emotions[i] < min)
			min = emotions[i];

	}
	cout << "total: " << total << endl;
	cout << "min: " << min << endl; //3590
	int num_train = min*separation;

	if (separation > 1 || separation < 0) {
		cout << "Separation must be between 0 and 1. Setting separation to 0.8" << endl;
		separation = 0.8;
	}

	/*bool any_empty = false;
	for (int i = 0; i < EMOTIONS_TO_TAKE; i++) {
		if(matrix[i].empty)
			any_empty = true;
	}
	while(any_empty == false) }*/
	for (int k = 0; k < min; k++) {

		for (int i = 0; i < EMOTIONS_TO_TAKE; i++) {
			int size = matrix[i].size();
			float n = rand() % size;
			float prob = rand() % 101 / 100;

			if (prob <= separation && count[i]<= num_train) {
				matrix_train.push_back(matrix[i][n]);
				count[i]++;
			}
			else {
				matrix_test.push_back(matrix[i][n]);
			}
			matrix[i].erase(matrix[i].begin()+n);

			/*if (matrix[i].empty)
				any_empty = true;*/
		}

		if (k % 100 == 0)
			cout << "sample " << k << " out of " << min << endl;

	}

}

int main(int argc, char *argv[])
{
	string paramsfile = "config_normalizeLandmarks3D_tospherical.txt";

	// Read parameters
	string* params;

	string modelfolder;
	string modelsubfolder;
	string file;
	params = new string[3];
	get_params(paramsfile, params, 3);
	modelfolder = params[0];
	modelsubfolder = params[1];
	file = params[2];
	cout << "model folder: " << modelfolder << endl;
	cout << "model subfolder: " << modelsubfolder << endl;
	cout << "file: " << file << endl;

	//get data from original csv file
	cout << "\ngetting data" << endl;
	vector<vector<string>> csvdata;
	get_data_from_csv(modelfolder + modelsubfolder + file + ".csv", csvdata);

	//to spherical coordinates
	cout << "\nto spherical coordinates" << endl;
	to_spherical(csvdata);

	//normalize spherical
	cout << "\nnormalizing landmarks" << endl;
	normalize_spherical(csvdata);

	//save data to new csv file
	cout << "\nsaving data" << endl;
	write_csv(modelfolder + modelsubfolder + file + "_spherical" + ".csv", csvdata);

	system("pause");
	return EXIT_SUCCESS;

}