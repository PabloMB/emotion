/*
 * superviseddescent: A C++11 implementation of the supervised descent
 *                    optimisation method
 * File: apps/rcr/rcr-detect.cpp
 *
 * Copyright 2015 Patrik Huber
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define _CRT_SECURE_NO_WARNINGS  //to avoid error C4996 with localtime (unsafe function)

#include "helpers.hpp"

#include "superviseddescent/superviseddescent.hpp"
#include "superviseddescent/regressors.hpp"

#include "rcr/landmarks_io.hpp"
#include "rcr/model.hpp"

#include "cereal/cereal.hpp"

#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/quaternion.hpp"

#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/objdetect/objdetect.hpp"

#include <dlib/image_processing/frontal_face_detector.h>
#include <dlib/opencv.h>

#include "boost/program_options.hpp"
#include "boost/filesystem.hpp"
#include "boost/algorithm/string.hpp"
#include "boost/property_tree/ptree.hpp"
#include "boost/property_tree/info_parser.hpp"

#include "Eigen/Core"
#include "eos/core/Image.hpp"
#include "eos/core/Image_opencv_interop.hpp"
#include "eos/core/Landmark.hpp"
#include "eos/core/LandmarkMapper.hpp"
#include "eos/core/read_pts_landmarks.hpp"
#include "eos/fitting/fitting.hpp"
#include "eos/fitting/RenderingParameters.hpp"
#include "eos/fitting/linear_shape_fitting.hpp"
#include "eos/fitting/orthographic_camera_estimation_linear.hpp"
#include "eos/morphablemodel/MorphableModel.hpp"
#include "eos/render/texture_extraction.hpp"
#include "eos/render/utils.hpp"
#include "eos/render/draw_utils.hpp"
//#include "eos/core/Mesh.hpp"
//#include "eos/fitting/fitting.hpp"
//#include "eos/fitting/contour_correspondence.hpp"
//#include "eos/fitting/closest_edge_fitting.hpp"
//#include "eos/render/render.hpp"
//#include "eos/render/draw_utils.hpp"
//#include "eos/core/Image_opencv_interop.hpp"
using namespace eos;
using eos::core::Landmark;
using eos::core::LandmarkCollection;
using Eigen::Vector2f;
using Eigen::Vector3f;
using Eigen::Vector4f;
using Eigen::VectorXf;
using Eigen::MatrixXf;

#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <cmath>

#include "utils.h"

using namespace superviseddescent;
namespace po = boost::program_options;
namespace fs = boost::filesystem;
namespace pt = boost::property_tree;
using std::vector;
using std::string;
using std::cout;
using std::endl;

#define MAX_NUM_LOAD 100

#define PI   3.1415926535897932384626433832795

/**
 * This app demonstrates the robust cascaded regression landmark detection from
 * "Random Cascaded-Regression Copse for Robust Facial Landmark Detection", 
 * Z. Feng, P. Huber, J. Kittler, W. Christmas, X.J. Wu,
 * IEEE Signal Processing Letters, Vol:22(1), 2015.
 *
 * It loads a model trained with rcr-train, detects a face using OpenCV's face
 * detector, and then runs the landmark detection.
 */

//void DrawPredictedImage(cv::Mat image, BoundingBox& bbox, cv::Mat_<double>& shape) {
//	unsigned long int t0, t1;
//	t0 = clock();
//	cv::rectangle(image, cv::Point(bbox.start_x, bbox.start_y), cv::Point(bbox.start_x + bbox.width, bbox.start_y + bbox.height), (255), 2);
//	for (int i = 0; i < shape.rows; i++) {
//		cv::circle(image, cv::Point2f(shape(i, 0), shape(i, 1)), 2, (255));
//	}
//	cv::imshow("show image", image);
//	t1 = clock();
//	std::cout << "time drawing faces: " << t1 - t0 << "ms" << std::endl;
//	cv::waitKey(0);
//}


//Loads all images and their corresponding .pts landmarks from the given directory
void load_data(vector<cv::Mat>& images,	string directory, string extension)
{
	int count = 0;

	// Get all the filenames in the given directory:
	vector<fs::path> image_filenames;
	fs::directory_iterator end_itr;
	for (fs::directory_iterator i(directory); i != end_itr; ++i)
	{
		if (fs::is_regular_file(i->status()) && i->path().extension() == extension) {
			image_filenames.emplace_back(i->path());
			count++;
			if (count >= MAX_NUM_LOAD)
				break;
		}
	}

	// Load each image
	for (auto file : image_filenames)
	{
		images.emplace_back(cv::imread(file.string()));
	}
};

//reads configuration file and load all images in the indicated folders
void load_data(vector<cv::Mat>& images, string config_file)
{
	std::ifstream fin;
	fin.open(config_file, std::ifstream::in);
	int num_datasets;
	fin >> num_datasets;

	string datapath, extension;
	for (int i = 0; i < num_datasets; i++) {
		fin >> datapath;
		fin >> extension;
		cout << "path " << datapath << " with extension " << extension << endl;
		load_data(images, datapath, extension);
		
	}
};

/**
* Reads a list of which landmarks to train.
*
* @param[in] configfile A training config file to read.
* @return A list that contains all the landmark identifiers from the config that are to be used for training.
*/
vector<string> read_landmarks_list_to_train(fs::path configfile)
{
	pt::ptree config_tree;
	pt::read_info(configfile.string(), config_tree); // Can throw a pt::ptree_error, maybe try/catch

	vector<string> model_landmarks;
	// Get stuff from the modelLandmarks subtree:
	pt::ptree pt_model_landmarks = config_tree.get_child("modelLandmarks");
	string model_landmarks_usage = pt_model_landmarks.get<string>("landmarks");
	if (model_landmarks_usage.empty()) {
		// value is empty, meaning it's a node and the user should specify a list of 'landmarks'
		pt::ptree pt_model_landmarks_list = pt_model_landmarks.get_child("landmarks");
		for (const auto& kv : pt_model_landmarks_list) {
			model_landmarks.push_back(kv.first);
		}
		cout << "Loaded a list of " << model_landmarks.size() << " landmarks to train the model." << endl;
	}
	else if (model_landmarks_usage == "all") {
		throw std::logic_error("Using 'all' modelLandmarks is not implemented yet - specify a list for now.");
	}
	else {
		throw std::logic_error("Error reading the models 'landmarks' key, should either provide a node with a list of landmarks or specify 'all'.");
	}
	return model_landmarks;
};

/**
* Reads a config file ('eval.txt') that specifies which landmarks make
* up the eyes and are to be used to calculate the IED (interEyedDistance).
*
* @param[in] evaluationfile A training config file to read.
* @return A pair with the right and left eye identifiers.
* @throws A ptree or logic error?
*/
std::pair<vector<string>, vector<string>> read_how_to_calculate_the_IED(fs::path evaluationfile)
{
	vector<string> right_eye_identifiers, left_eye_identifiers;

	pt::ptree eval_config_tree;
	string right_eye;
	string left_eye;
	pt::read_info(evaluationfile.string(), eval_config_tree); // could throw a boost::property_tree::ptree_error, maybe try/catch

	pt::ptree pt_parameters = eval_config_tree.get_child("interEyeDistance");
	right_eye = pt_parameters.get<string>("rightEye");
	left_eye = pt_parameters.get<string>("leftEye");

	// Process the interEyeDistance landmarks - one or two identifiers might be given
	boost::split(right_eye_identifiers, right_eye, boost::is_any_of(" "));
	boost::split(left_eye_identifiers, left_eye, boost::is_any_of(" "));
	return std::make_pair(right_eye_identifiers, left_eye_identifiers);
}

void draw_axes_topright(float r_x, float r_y, float r_z, cv::Mat image) //inputs are euler angles
{
	/*const float r_x_degrees = glm::degrees(r_x);
	const float r_y_degrees = glm::degrees(r_y);
	const float r_z_degrees = glm::degrees(r_z);
	cout << "\t\tr_x: " << r_x_degrees << " (" << r_x << ")" << endl;
	cout << "\t\tr_y: " << r_y_degrees << " (" << r_y << ")" << endl;
	cout << "\t\tr_z: " << r_z_degrees << " (" << r_z << ")" << endl;*/

	const glm::vec3 origin(0.0f, 0.0f, 0.0f);
	const glm::vec3 x_axis(1.0f, 0.0f, 0.0f);
	const glm::vec3 y_axis(0.0f, 1.0f, 0.0f);
	const glm::vec3 z_axis(0.0f, 0.0f, 1.0f);

	const auto rot_mtx_x = glm::rotate(glm::mat4(1.0f), r_x, glm::vec3{ 1.0f, 0.0f, 0.0f });
	const auto rot_mtx_y = glm::rotate(glm::mat4(1.0f), r_y, glm::vec3{ 0.0f, 1.0f, 0.0f });
	const auto rot_mtx_z = glm::rotate(glm::mat4(1.0f), r_z, glm::vec3{ 0.0f, 0.0f, 1.0f });
	const auto modelview = rot_mtx_z * rot_mtx_x * rot_mtx_y;

	const auto viewport = fitting::get_opencv_viewport(image.cols, image.rows);
	const float aspect = static_cast<float>(image.cols) / image.rows;
	const auto ortho_projection = glm::ortho(-3.0f * aspect, 3.0f * aspect, -3.0f, 3.0f);
	const auto translate_topright = glm::translate(glm::mat4(1.0f), glm::vec3(0.7f, 0.65f, 0.0f));
	const auto o_2d = glm::project(origin, modelview, translate_topright * ortho_projection, viewport);
	const auto x_2d = glm::project(x_axis, modelview, translate_topright * ortho_projection, viewport);
	const auto y_2d = glm::project(y_axis, modelview, translate_topright * ortho_projection, viewport);
	const auto z_2d = glm::project(z_axis, modelview, translate_topright * ortho_projection, viewport);
	cv::line(image, cv::Point2f{ o_2d.x, o_2d.y }, cv::Point2f{ x_2d.x, x_2d.y }, { 0, 0, 255 });
	cv::line(image, cv::Point2f{ o_2d.x, o_2d.y }, cv::Point2f{ y_2d.x, y_2d.y }, { 0, 255, 0 });
	cv::line(image, cv::Point2f{ o_2d.x, o_2d.y }, cv::Point2f{ z_2d.x, z_2d.y }, { 255, 0, 0 });
};

inline cv::Mat to_col(vector<Vector2f> landmarks)
{
	// landmarks.size() must be <= max_int
	auto num_landmarks = static_cast<int>(landmarks.size());
	cv::Mat col(num_landmarks * 2, 1, CV_32FC1);
	for (int i = 0; i < num_landmarks; ++i) {
		col.at<float>(i) = landmarks[i].x();
		col.at<float>(i + num_landmarks) = landmarks[i].y();
	}
	return col;
}
inline cv::Mat to_col(vector<Vector3f> landmarks)
{
	// landmarks.size() must be <= max_int
	auto num_landmarks = static_cast<int>(landmarks.size());
	cv::Mat col(num_landmarks * 3, 1, CV_32FC1);
	for (int i = 0; i < num_landmarks; ++i) {
		col.at<float>(i) = landmarks[i].x();
		col.at<float>(i + num_landmarks) = landmarks[i].y();
		col.at<float>(i + num_landmarks * 2) = landmarks[i].z();
	}
	return col;
}


int main(int argc, char *argv[])
{
	/*fs::path facedetector, paramsfile, testing_config_file, outputfile;
	try {
		po::options_description desc("Allowed options");
		desc.add_options()
			("help,h",
				"display the help message")
			("facedetector,f", po::value<fs::path>(&facedetector)->required()->default_value("data/haarcascade_frontalface_alt2.xml"),
				"full path to OpenCV's face detector (haarcascade_frontalface_alt2.xml)")
			("params,p", po::value<fs::path>(&paramsfile)->required()->default_value("config_testing.txt"),
				"parameters for the program")
			("image,i", po::value<fs::path>(&testing_config_file)->required()->default_value("config_testingset.txt"),
				"input image file")
			("output,o", po::value<fs::path>(&outputfile)->required()->default_value("results/296961468_1_result.jpg"),
				"filename for the result image")
			;
		po::variables_map vm;
		po::store(po::command_line_parser(argc, argv).options(desc).run(), vm);
		if (vm.count("help")) {
			cout << "Usage: rcr-detect [options]" << endl;
			cout << desc;
			system("pause");
			return EXIT_SUCCESS;
		}
		po::notify(vm);
	}
	catch (const po::error& e) {
		cout << "Error while parsing command-line arguments: " << e.what() << endl;
		cout << "Use --help to display a list of options." << endl;
		system("pause");
		return EXIT_SUCCESS;
	}*/

	string facedetector, paramsfile, testing_config_file, outputfile;
	facedetector = "data/haarcascade_frontalface_alt2.xml";
	paramsfile = "config_testing.txt";
	testing_config_file = "config_testingset.txt";
	outputfile = "results/296961468_1_result.jpg";

	// Read parameters
	string* params;

	string folder;
	string modelfile;
	params = new string[2];
	get_params(paramsfile, params, 2);
	folder = params[0];
	modelfile = params[1];
	cout << "folder: " << folder << endl;
	cout << "model file: " << modelfile << endl;

	string selection;
	params = new string[1];
	get_params(folder+"model_params.txt", params, 1);
	selection = params[0];
	cout << "selection: " << selection << endl;

	// Load the learned model:
	rcr::detection_model rcr_model;
	try {
		rcr_model = rcr::load_detection_model(folder + modelfile);
	}
	catch (const cereal::Exception& e) {
		cout << "Error reading the RCR model " << folder + modelfile << ": " << e.what() << endl;
		system("pause");
		return EXIT_FAILURE;
	}

	// Load the face detector from OpenCV:
	cv::CascadeClassifier face_cascade;
	if (!face_cascade.load(facedetector))
	{
		cout << "Error loading the face detector " << facedetector << "." << endl;
		system("pause");
		return EXIT_FAILURE;
	}
	dlib::frontal_face_detector detector_dlib = dlib::get_frontal_face_detector();

	core::LandmarkMapper landmark_mapper;
	string mappingsfile = "data/ibug_to_sfm.txt";
	try
	{
		landmark_mapper = core::LandmarkMapper(mappingsfile);
	}
	catch (const std::exception& e)
	{
		cout << "Error loading the landmark mappings: " << e.what() << endl;
		system("pause");
		return EXIT_FAILURE;
	}

	morphablemodel::MorphableModel morphable_model;
	string morphablemodelfile = "data/sfm_shape_3448.bin";
	try
	{
		morphable_model = morphablemodel::load_model(morphablemodelfile);
	}
	catch (const std::runtime_error& e)
	{
		cout << "Error loading the Morphable Model: " << e.what() << endl;
		system("pause");
		return EXIT_FAILURE;
	}

	vector<string> model_landmarks; // list read from the files, might be 68 or less
	try {
		model_landmarks = read_landmarks_list_to_train("data/rcr_training_68.cfg");
	}
	catch (const pt::ptree_error& e) {
		cout << "Error reading the training config: " << e.what() << endl;
		system("pause");
		return EXIT_FAILURE;
	}
	catch (const std::logic_error& e) {
		cout << "Parsing config: " << e.what() << endl;
		system("pause");
		return EXIT_FAILURE;
	}

	vector<string> right_eye_identifiers, left_eye_identifiers; // for ied calc. One or several.
	try {
		std::tie(right_eye_identifiers, left_eye_identifiers) = read_how_to_calculate_the_IED("data/rcr_eval.cfg");
	}
	catch (const pt::ptree_error& e) {
		cout << "Error reading the evaluation config: " << e.what() << endl;
		system("pause");
		return EXIT_FAILURE;
	}
	catch (const std::logic_error& e) {
		cout << "Parsing config: " << e.what() << endl;
		system("pause");
		return EXIT_FAILURE;
	}

	cout << "loading blendshapes2" << endl;
	const morphablemodel::Blendshapes blendshapes0 = morphablemodel::load_blendshapes("data/expression_blendshapes_3448.bin");
	cout << "loading edge_topology2" << endl;
	const morphablemodel::EdgeTopology edge_topology0 = morphablemodel::load_edge_topology("data/sfm_3448_edge_topology.json");
	cout << "loading model_contour2" << endl;
	const fitting::ModelContour model_contour0 = fitting::ModelContour::load("data/sfm_model_contours.json");
	cout << "loading ibug_contour2" << endl;
	const fitting::ContourLandmarks ibug_contour0 = fitting::ContourLandmarks::load("data/ibug_to_sfm.txt");



	vector<cv::Mat> images;
	load_data(images, testing_config_file);
	if (images.empty()) {
		cout << "ERROR: no images found" << endl;
		system("pause");
		return -1;
	}

	unsigned long int t0, t1;
	for (int i = 0; i < images.size(); i++) {

		myresize(images[i]);

		cv::Mat blank = images[i].clone();
		blank = cv::Scalar(255.0, 255.0, 255.0); //BGR
		
		cv::Mat_<uchar> image_gray;
		cvtColor(images[i], image_gray, CV_BGR2GRAY);

		//use OpenCV detector or dlib detector
		vector<cv::Rect> detected_faces;
		if (selection == "opencv") {
			unsigned long int t0, t1;
			t0 = clock();
			face_cascade.detectMultiScale(image_gray, detected_faces, 1.2, 2, 0, cv::Size(50, 50));
			t1 = clock();
			cout << "\ntime detecting faces with opencv: " << t1 - t0 << " ms" << endl;
		}
		else {
			//dlib::array2d<dlib::bgr_pixel> image_dlib_color;
			//dlib::assign_image(image_dlib_color, dlib::cv_image<dlib::bgr_pixel>(images[i]));

			dlib::array2d<unsigned char> image_dlib;
			dlib::assign_image(image_dlib, dlib::cv_image<unsigned char>(image_gray));

			//load_image(image_dlib, image_path.c_str());
			vector<dlib::rectangle> dets;
			unsigned long int t0, t1;
			t0 = clock();
			dets = detector_dlib(image_dlib);
			t1 = clock();
			cout << "\ntime detecting faces with dlib: " << t1 - t0 << " ms" << endl;
			for (int i = 0; i < dets.size(); i++) {
				detected_faces.push_back(dlibRectangleToOpenCV(dets[i]));
			}
		}

		if (detected_faces.empty()) {
			cout << "\tNO FACES DETECTED" << endl;
		}
		else {
			cout << "image: " << images[i].cols << " x " << images[i].rows << endl;
			for (int j = 0; j < detected_faces.size(); j++) {
				cout << "rectangle: (" << detected_faces[j].x << "," << detected_faces[j].y << ") + " << detected_faces[j].width << "x" << detected_faces[j].height << endl;
				//Draw rectangles for the detected faces
				cv::rectangle(images[i], detected_faces[j], cv::Scalar(0, 0, 255), 5);
				// Detect the landmarks:
				t0 = clock();
				auto landmarks_collection = rcr_model.detect(images[i], detected_faces[j]);
				t1 = clock();
				cout << "\ttime detecting landmarks: " << t1 - t0 << " ms" << endl;
				//Draw the landmarks on the image
				cv::Mat landmarks_mat = to_row(landmarks_collection);
				cv::Mat landmarks_col = to_col(landmarks_collection);
				//rcr::draw_landmarks(images[i], landmarks_collection);

				// FIT THE 3DMM:
				t0 = clock();
				fitting::RenderingParameters rendering_params2;
				vector<float> shape_coefficients2, blendshape_coefficients2;
				vector<Eigen::Vector2f> image_points2;
				core::Mesh mesh2;
				std::tie(mesh2, rendering_params2) = eos::fitting::fit_shape_and_pose(morphable_model, blendshapes0, rcr_to_eos_landmark_collection(landmarks_collection),
					landmark_mapper, images[i].cols, images[i].rows, edge_topology0,
					ibug_contour0, model_contour0, 3, 5, 15.0f, cpp17::nullopt, shape_coefficients2,
					blendshape_coefficients2,
					image_points2); //image_points is output
				t1 = clock();
				cout << "\t\ttime fitting the 3D model: " << t1 - t0 << " ms" << endl;

				//GETTING 3D COORDINATES (with the closest point in the 3D mesh to the landmarks)
				vector<Vector3f> mesh3D = mesh2.vertices;
				t0 = clock();
				vector<Vector3f> mesh3D_proj; //vertices3D projected
				for (int k = 0; k < mesh3D.size(); k++) {
					auto p = glm::project({ mesh3D[k][0], mesh3D[k][1], mesh3D[k][2] }, rendering_params2.get_modelview(), rendering_params2.get_projection(), fitting::get_opencv_viewport(images[i].cols, images[i].rows));
					mesh3D_proj.push_back({ p.x, p.y, p.z });
				}
				vector<Vector3f> landmarks3D_proj;
				int landmarks_num = landmarks_col.rows / 2;
				for (int k = 0; k < landmarks_num; k++) {
					landmarks3D_proj.push_back({ landmarks_col.at<float>(k), landmarks_col.at<float>(k + landmarks_num), 0.0f });
				}
				vector<Eigen::Vector2i> indices;
				for (int m = 0; m < landmarks3D_proj.size(); m++) {
					float min = 10000;
					//float ind = -1;
					float z = 0;
					for (int n = 0; n < mesh3D_proj.size(); n++) {
						Vector2f v1(landmarks3D_proj[m].x(), landmarks3D_proj[m].y());
						Vector2f v2(mesh3D_proj[n].x(), mesh3D_proj[n].y());
						//float norm2 = (v1 - v2).norm(); //8ms
						float norm1 = abs(v1.x() - v2.x()) + abs(v1.y() - v2.y());
						if (norm1 < min) {
							min = norm1;
							//ind = n;
							z = mesh3D_proj[n].z();
						}
					}
					//indices.push_back({ m,ind });
					landmarks3D_proj[m](2) = z;
				}
				vector<Vector2f> landmarks2D;
				vector<Vector3f> landmarks3D; //vertices3D projected
				for (int k = 0; k < landmarks3D_proj.size(); k++) { //unproject landmarks3D_proj
					auto p = glm::unProject({ landmarks3D_proj[k][0], landmarks3D_proj[k][1], landmarks3D_proj[k][2] }, rendering_params2.get_modelview(), rendering_params2.get_projection(), fitting::get_opencv_viewport(images[i].cols, images[i].rows));
					landmarks3D.push_back({ p.x, p.y, p.z });
					float qx = p.x*cos(PI) - p.y*sin(PI);
					float qy = p.x*sin(PI) + p.y*cos(PI);
					landmarks2D.push_back({ qx, qy });
				}
				t1 = clock(); cout << "\t\ttime getting z coordinates from mesh: " << t1 - t0 << " ms" << endl;
				//for (int m = 0; m < landmarks3D_proj.size(); m++) {
				//	landmarks3D_proj[m][1] = image.rows - landmarks3D_proj[m][1]; //change y axis direction
				//}
				//std::ofstream myfile;
				//myfile.open("results/correspondences"+std::to_string(count)+".csv");
				//for (int k = 0; k < indices.size(); k++) {
				//	//cout << indices[k][0]+1 << "," << indices[k][1] << endl;
				//	myfile << indices[k][0]+1 << "," << indices[k][1] << endl;
				//}
				//myfile.close();
				/*myfile.open("results/3Dcoordinates_proj_" + std::to_string(count) + ".csv");
				for (int k = 0; k < landmarks3D_proj.size(); k++) {
					myfile << landmarks3D_proj[k][0] << "," << landmarks3D_proj[k][1] << "," << landmarks3D_proj[k][2] << endl;
				}
				myfile.close();
				myfile.open("results/3Dcoordinates_" + std::to_string(count) + ".csv");
				for (int k = 0; k < landmarks3D.size(); k++) {
					myfile << landmarks3D[k][0] << "," << landmarks3D[k][1] << "," << landmarks3D[k][2] << endl;
				}
				myfile.close();*/
				//cv::Mat landmarks3D_col = to_col(landmarks3D);
				//cv::Mat landmarks2D_col = to_col(landmarks2D);

				cv::rectangle(images[i], detected_faces[j], cv::Scalar(0, 0, 255), 2); //BGR
				//render::draw_wireframe(images[i], mesh2, rendering_params2.get_modelview(), rendering_params2.get_projection(), fitting::get_opencv_viewport(image.cols, image.rows));
				rcr::draw_landmarks(images[i], landmarks_collection, cv::Scalar(255, 0, 0)); //landmarks extracted in blue
				//auto landmarks_tr_collection = rcr::to_landmark_collection(landmarks_tr_row, model_landmarks);
				//rcr::draw_landmarks(images[i], landmarks_tr_collection, cv::Scalar(255, 0, 255)); //true landmarks in purple
				//draw_axes_topright(pitch_radians, yaw_radians, roll_radians, image);

				float max_y = -images[i].rows;
				float max_x = -images[i].cols;
				float min_y = images[i].rows;
				float min_x = images[i].cols;
				for (int k = 0; k < landmarks2D.size(); k++) {
					float x = landmarks2D[k][0];
					float y = landmarks2D[k][1];
					if (x > max_x)
						max_x = x;
					if (y > max_y)
						max_y = y;
					if (x < min_x)
						min_x = x;
					if (y < min_y)
						min_y = y;
				}
				for (int k = 0; k < landmarks2D.size(); k++) {
					//cout << landmarks2D[k][0] << "-" << min_x << "  -> ";
					landmarks2D[k](0) = landmarks2D[k][0] - min_x;
					landmarks2D[k](1) = landmarks2D[k][1] - min_y;
					//cout << landmarks2D[k](0) << endl;
				}
				cv::Mat landmarks2D_col = to_col(landmarks2D);
				//blank = cv::Mat(max_y-min_y, max_x-min_x, CV_32FC1);
				//blank = cv::Mat(max_y, max_x, CV_32FC1);
				cv::resize(blank, blank, cv::Size(max_x - min_x, max_y-min_y), 0, 0, cv::INTER_LINEAR);
				rcr::draw_landmarks(blank, landmarks2D_col, cv::Scalar(255, 0, 0), 2); //landmarks x and y from 3D landmarks extracted in blue
			}
		}

		//cv::imwrite(outputfile.string(), images[i]);
		cv::imshow("show image", images[i]);
		cv::imshow("show result", blank);
		cv::waitKey(0);
	}


	//system("pause");
	return EXIT_SUCCESS;
}
