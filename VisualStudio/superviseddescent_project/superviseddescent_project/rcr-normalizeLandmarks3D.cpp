
#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <stdlib.h>     /* srand, rand */

#include "utils.h"

using std::vector;
using std::string;
using std::cout;
using std::endl;

#define LANDMARKS_NUM 68
#define EMOTIONS_NUM 11 //FROM 0 TO 10
#define EMOTIONS_TO_TAKE 8 //FROM 0 TO 7
#define PI   3.1415926535897932384626433832795
#define PRINT_EVERY_N 10000

void get_data_from_csv(const string filename, vector<vector<string>>& matrix) {
	std::ifstream inputfile(filename);
	string current_line;
	matrix.clear();

	while (getline(inputfile, current_line)) {
		vector<string> vect;
		std::stringstream temp(current_line);
		string single_value;
		while (getline(temp, single_value, ',')) {
			vect.push_back(single_value);
		}
		matrix.push_back(vect);
	}
};

void write_csv(const string filename, const vector<vector<string>>& matrix)
{
	std::ofstream myfile;
	myfile.open(filename.c_str());
	

	for (int i = 0; i < matrix.size(); i++) {
		if (matrix[i].size() >= LANDMARKS_NUM * 2) {

			for (int j = 0; j < matrix[i].size(); j++) {
				myfile << matrix[i][j];
				if (j != matrix[i].size() - 1)
					myfile << ",";
			}
			myfile << endl;

		}
	}

	myfile.close();
}

void normalize_landmarks(vector<string>& vector)
{
	//find max and min of x coordinates and of y coordinates
	double x_max, x_min, y_max, y_min, z_max, z_min, x_center, y_center, z_center;
	x_center = stod(vector[34-1]);
	y_center = stod(vector[34-1+LANDMARKS_NUM]);
	z_center = stod(vector[34-1+LANDMARKS_NUM*2]);
	x_max = x_min = x_center;
	y_max = y_min = y_center;
	z_max = z_min = z_center;
	for (int j = 0; j < LANDMARKS_NUM*3; j++) {
		double value = stod(vector[j]);
		if (j < LANDMARKS_NUM) { //landmarks that correspond to x
			if (value > x_max)
				x_max = value;
			if (value < x_min)
				x_min = value;
		}
		else if (j < LANDMARKS_NUM*2) { //landmarks that correspond to y
			if (value > y_max)
				y_max = value;
			if (value < y_min)
				y_min = value;
		}
		else { //landmarks that correspond to z
			if (value > z_max)
				z_max = value;
			if (value < z_min)
				z_min = value;
		}
	}
	/*cout << "\tx_max: " << x_max << endl;
	cout << "\tx_min: " << x_min << endl;
	cout << "\ty_max: " << y_max << endl;
	cout << "\ty_min: " << y_min << endl;*/

	//find max length to divide all landmarks with that length so that they are between -1 and 1, being the tip of the nose the center
	double divx = std::max(abs(x_max - x_center), abs(x_min - x_center));
	double divy = std::max(abs(y_max - y_center), abs(y_min - y_center)); //abs may not be needed if we know the orientation of the axes
	double divz = std::max(abs(z_max - z_center), abs(z_min - z_center));

	//scale all landmarks with that rate
	for (int j = 0; j < LANDMARKS_NUM * 3; j++) {
		double value = stod(vector[j]);
		if (j < LANDMARKS_NUM) { //landmarks that correspond to x
			value -= x_center;
			value /= divx; //this line: to obtain landmarks from -1 to 1
			value += 1;    //this line: to obtain landmarks from 0 to 2 instead of from -1 to 1
			value /= 2;    //this line: to obtain landmarks from 0 to 1 instead of from 0 to 2
		}
		else if (j < LANDMARKS_NUM * 2) { //landmarks that correspond to y
			value -= y_center;
			value /= divy; //this line: to obtain landmarks from -1 to 1
			value += 1;    //this line: to obtain landmarks from 0 to 2 instead of from -1 to 1
			value /= 2;    //this line: to obtain landmarks from 0 to 1 instead of from 0 to 2
		}
		else { //landmarks that correspond to z
			value -= z_center;
			value /= divz; //this line: to obtain landmarks from -1 to 1
			value += 1;    //this line: to obtain landmarks from 0 to 2 instead of from -1 to 1
			value /= 2;    //this line: to obtain landmarks from 0 to 1 instead of from 0 to 2
		}
		vector[j] = std::to_string(value);
	}

}

void normalize_landmarks(vector<vector<string>>& matrix)
{
	for (int i = 0; i < matrix.size(); i++) {
		if (matrix[i].size() >= LANDMARKS_NUM * 2) {
			normalize_landmarks(matrix[i]);
			if (i % PRINT_EVERY_N == 0 )
				cout << "line: " << i << endl;
		}
	}
}

void normalize_angles(vector<string>& vector)
{
	//scale all angles to be between 0 and 1 (initially they are between -2PI and 2PI )
	for (int j = 0; j < 3; j++) {
		int index = LANDMARKS_NUM * 2 + j;
		double value = stod(vector[index]);
		//cout << value << " to ";
		value = (value + PI) / (2*PI);
		//cout << value << endl;
		vector[index] = std::to_string(value);
	}

}

void normalize_angles(vector<vector<string>>& matrix)
{
	for (int i = 0; i < matrix.size(); i++) {
		if (matrix[i].size() >= LANDMARKS_NUM * 2) {
			normalize_angles(matrix[i]);
			if (i % PRINT_EVERY_N == 0)
				cout << "line: " << i << endl;
		}
	}
}

void add_ones(vector<vector<string>>& matrix)
{
	for (int i = 0; i < matrix.size(); i++) {
		if (matrix[i].size() >= LANDMARKS_NUM * 2) {
			if (i % PRINT_EVERY_N == 0)
				cout << "line: " << i << endl;
			for (int j = 0; j < 11; j++)
				matrix[i].push_back("0");
			int val = atoi(matrix[i][LANDMARKS_NUM*2+3].c_str());
			matrix[i][LANDMARKS_NUM*2+3 + 1 + val] = "1";
		}
	}
}

void delete_emotions(vector<vector<string>>& matrix_origin, vector<vector<string>>& matrix_final) {

	int ei = LANDMARKS_NUM * 3; //emotion index
	for (int i = 0; i < matrix_origin.size(); i++) {
		if (i % PRINT_EVERY_N == 0)
			cout << "line: " << i << endl;
		int val = atoi(matrix_origin[i][ei].c_str());
		if (val < EMOTIONS_TO_TAKE) { //only the first 8 emotions
			matrix_final.push_back(matrix_origin[i]);
		}
	}
}

void divide_data(vector<vector<string>>& matrix_origin, vector<vector<string>>& matrix_train, vector<vector<string>>& matrix_test, float separation) {
	int num_train = matrix_origin.size()*separation;
	int count = 0;
	srand (time(NULL));

	if (separation > 1 || separation < 0) {
		cout << "Separation must be between 0 and 1. Setting separation to 0.5" << endl;
		separation = 0.5;
	}
	for (int i = 0; i < matrix_origin.size(); i++) {
		if (i % PRINT_EVERY_N == 0)
			cout << "line: " << i << endl;

		float prob = rand() % 101 / 100;
		if (prob <= separation && count <= num_train) {
			matrix_train.push_back(matrix_origin[i]);
			count++;
		}
		else
			matrix_test.push_back(matrix_origin[i]);
	}
}

void divide_data2(vector<vector<string>>& matrix_origin, vector<vector<string>>& matrix_train, vector<vector<string>>& matrix_test, float separation) {

	srand(time(NULL));

	//count emotions of each type
	int emotions[EMOTIONS_TO_TAKE];
	int count[EMOTIONS_TO_TAKE];
	for (int i = 0; i < EMOTIONS_TO_TAKE; i++) {
		emotions[i] = 0;
		count[i] = 0;
	}
	int ei = LANDMARKS_NUM * 3; //emotion index
	int emotion;
	int total = 0;
	vector<vector<string>> matrix[EMOTIONS_TO_TAKE];
	for (int i = 0; i < matrix_origin.size(); i++) {
		emotion = atoi(matrix_origin[i][ei].c_str());
		emotions[emotion]++;
		total++;
		matrix[emotion].push_back(matrix_origin[i]);
	}
	int min = 1000000;
	for (int i = 0; i < EMOTIONS_TO_TAKE; i++) {
		cout << "emotion " << i << ": " << emotions[i] << "(" << emotions[i] * 100 / total << "%)" << endl;
		if (emotions[i] < min)
			min = emotions[i];

	}
	cout << "total: " << total << endl;
	cout << "min: " << min << endl; //3590
	int num_train = min*separation;

	if (separation > 1 || separation < 0) {
		cout << "Separation must be between 0 and 1. Setting separation to 0.8" << endl;
		separation = 0.8;
	}

	/*bool any_empty = false;
	for (int i = 0; i < EMOTIONS_TO_TAKE; i++) {
		if(matrix[i].empty)
			any_empty = true;
	}
	while(any_empty == false) }*/
	for (int k = 0; k < min; k++) {

		for (int i = 0; i < EMOTIONS_TO_TAKE; i++) {
			int size = matrix[i].size();
			float n = rand() % size;
			float prob = rand() % 101 / 100;

			if (prob <= separation && count[i]<= num_train) {
				matrix_train.push_back(matrix[i][n]);
				count[i]++;
			}
			else {
				matrix_test.push_back(matrix[i][n]);
			}
			matrix[i].erase(matrix[i].begin()+n);

			/*if (matrix[i].empty)
				any_empty = true;*/
		}

		if (k % 100 == 0)
			cout << "sample " << k << " out of " << min << endl;

	}

}

int main(int argc, char *argv[])
{
	string paramsfile = "config_normalizeLandmarks3D.txt";

	// Read parameters
	string* params;

	string modelfolder;
	string modelsubfolder;
	params = new string[2];
	get_params(paramsfile, params, 2);
	modelfolder = params[0];
	modelsubfolder = params[1];
	cout << "model folder: " << modelfolder << endl;


	//get data from original csv file
	cout << "\ngetting data" << endl;
	vector<vector<string>> csvdata;
	get_data_from_csv(modelfolder + modelsubfolder + "results.csv", csvdata);

	//keep only the emotions we want
	cout << "\nkeep only the emotions we want" << endl;
	vector<vector<string>> csvdata8;
	delete_emotions(csvdata, csvdata8);

	//normalize landmarks
	cout << "\nnormalizing landmarks" << endl;
	normalize_landmarks(csvdata8);

	////normalize angles
	//cout << "\nnormalizing angles" << endl;
	//normalize_angles(csvdata8);

	//adding ones
	//cout << "\nadding ones" << endl;
	//add_ones(csvdata8);

	//randomly divide in two
	cout << "\ndividing dataset" << endl;
	vector<vector<string>> csvdata8_train;
	vector<vector<string>> csvdata8_test;
	divide_data(csvdata8, csvdata8_train, csvdata8_test, 0.8);		//results in not balanced
	//divide_data2(csvdata8, csvdata8_train, csvdata8_test, 0.8);	//results in balanced datasets

	//save data to new csv file
	cout << "\nsaving data" << endl;
	write_csv(modelfolder + modelsubfolder + "results_8emotions_normalized_0to1_all.csv", csvdata8);
	write_csv(modelfolder + modelsubfolder + "results_8emotions_normalized_0to1_train.csv", csvdata8_train);
	write_csv(modelfolder + modelsubfolder + "results_8emotions_normalized_0to1_test.csv", csvdata8_test);


	system("pause");
	return EXIT_SUCCESS;

}