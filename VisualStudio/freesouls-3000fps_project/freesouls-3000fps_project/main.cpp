#include <opencv2/opencv.hpp>
#include <iostream>
#include <fstream>
#include <string>
#include <time.h>
//#include <Windows.h>

#define DLIB
#include <dlib/image_processing/frontal_face_detector.h>

#include "headers.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <omp.h>

using namespace cv;
using namespace dlib;
using namespace std;

void DrawPredictedImage(cv::Mat_<uchar> image, cv::Mat_<double>& shape){
	unsigned long int t0, t1; 
	t0 = clock();
	for (int i = 0; i < shape.rows; i++){
		cv::circle(image, cv::Point2f(shape(i, 0), shape(i, 1)), 2, (255));
	}
	cv::imshow("show image", image);
	t1 = clock();
	std::cout << "time drawing faces: " << t1 - t0 << "ms" << std::endl;
	cv::waitKey(0);
}

void DrawPredictedImage(cv::Mat_<uchar> image, BoundingBox& bbox, cv::Mat_<double>& shape) {
	unsigned long int t0, t1;
	t0 = clock();
	cv::rectangle(image, cv::Point(bbox.start_x, bbox.start_y), cv::Point(bbox.start_x + bbox.width, bbox.start_y + bbox.height), (255), 2);
	for (int i = 0; i < shape.rows; i++) {
		cv::circle(image, cv::Point2f(shape(i, 0), shape(i, 1)), 2, (255));
	}
	cv::imshow("show image", image);
	t1 = clock();
	std::cout << "time drawing faces: " << t1 - t0 << "ms" << std::endl;
	cv::waitKey(0);
}

void DrawPredictedImage(cv::Mat_<cv::Vec3b> image, BoundingBox& bbox, cv::Mat_<double>& shape) {
	unsigned long int t0, t1;
	t0 = clock();
	cv::rectangle(image, cv::Point(bbox.start_x, bbox.start_y), cv::Point(bbox.start_x + bbox.width, bbox.start_y + bbox.height), (255), 2);
	for (int i = 0; i < shape.rows; i++) {
		cv::circle(image, cv::Point2f(shape(i, 0), shape(i, 1)), 2, (255));
	}
	cv::imshow("show image", image);
	t1 = clock();
	std::cout << "time drawing faces: " << t1 - t0 << "ms" << std::endl;
	cv::waitKey(0);
}

void DrawPredictedImage(cv::Mat_<uchar> image, BoundingBox& bbox) {
	unsigned long int t0, t1;
	t0 = clock();
	cv::rectangle(image, cv::Point(bbox.start_x, bbox.start_y), cv::Point(bbox.start_x + bbox.width, bbox.start_y + bbox.height), (255), 2);
	cv::imshow("show image", image);
	t1 = clock();
	std::cout << "time drawing faces: " << t1 - t0 << "ms" << std::endl;
	cv::waitKey(0);
}

void drawOne(cv::Mat_<uchar>& image, BoundingBox& bbox, cv::Mat_<double>& shape) {
	cv::rectangle(image, cv::Point(bbox.start_x, bbox.start_y), cv::Point(bbox.start_x + bbox.width, bbox.start_y + bbox.height), cv::Scalar(0, 0, 255), 3);
	for (int i = 0; i < shape.rows; i++) {
		cv::circle(image, cv::Point2f(shape(i, 0), shape(i, 1)), 2, (255));
	}
}

void TestWebcam(string selection) {

	std::cout << "\nLoading model..." << std::endl;
	CascadeRegressor cas_load;
	cas_load.LoadCascadeRegressor("./example/helen_trained_model/helenModel"); // !!!!!!This takes a long time!!!!!!
	cout << "load model done\n" << endl;

	std::cout << "\nLoading face detector..." << std::endl;
	std::string fn_haar = "data/haarcascade_frontalface_alt2.xml";
	cv::CascadeClassifier haar_cascade;
	bool yes = haar_cascade.load(fn_haar);
	std::cout << "face detector loaded : " << yes << std::endl;

	frontal_face_detector detector_dlib = get_frontal_face_detector();

	std::cout << "\nLoading webcam..." << std::endl;
	cv::Mat_<uchar> capture, capture_gray;
	VideoCapture camera(0);
	string window_name = "Webcam";
	cout << "webcam open" << endl;
	std::vector<BoundingBox> bboxes;
	cv::Mat_<double> current_shape;
	cv::Mat_<double> res;

	unsigned long int t0, t1, tcycle, cycles=0;
	
	t0 = clock();
	camera >> capture;
	cvtColor(capture, capture_gray, CV_BGR2GRAY);
	//cap = cv::imread("data/face.jpg", 0);
	while (!capture.empty()) {
		bboxes.clear();
		cout << "detecting" << endl;
		detectFaces(capture_gray, bboxes, haar_cascade, detector_dlib, selection); //loads images and detects faces
		for (size_t i = 0; i < bboxes.size(); i++) {
			current_shape = ReProjection(cas_load.params_.mean_shape_, bboxes[i]);
			res = cas_load.Predict(capture_gray, current_shape, bboxes[i]); //predicts landmarks
			drawOne(capture, bboxes[i], res);
		}
		imshow(window_name, capture);
		waitKey(1);

		cycles++;
		t1 = clock();
		tcycle = t1 - t0;
		if (tcycle > 1000) {
			cout << "\t\t" << float(cycles)/tcycle*1000 << " cycles/second" << endl;
			cycles = 0;
			t0 = clock();
		}

		camera >> capture;
		cvtColor(capture, capture_gray, CV_BGR2GRAY);
	}

	destroyAllWindows();
	camera.release();
	cout << "webcam closed" << endl;

}

void TestImage(const char* config_file_path, string selection) {

	unsigned long int t0, t1, t2, t3;
	t0 = clock();

	cout << "parsing config_file: " << config_file_path << endl;

	ifstream fin;
	fin.open(config_file_path, ifstream::in);
	std::string model_name;
	fin >> model_name;
	cout << "model name is: " << model_name << endl;
	bool images_has_ground_truth = false;
	fin >> images_has_ground_truth;
	if (images_has_ground_truth) {
		cout << "the image lists must have ground_truth_shapes!\n" << endl;
	}
	else {
		cout << "the image lists does not have ground_truth_shapes!!!\n" << endl;
	}

	int path_num;
	fin >> path_num;
	cout << "reading testing images paths: " << endl;
	std::vector<std::string> image_path_prefixes;
	std::vector<std::string> image_lists;
	for (int i = 0; i < path_num; i++) {
		string s;
		fin >> s;
		cout << s << endl;
		image_path_prefixes.push_back(s);
		fin >> s;
		cout << s << endl;
		image_lists.push_back(s);
	}
	cout << "parsing config file done\n" << endl;

	std::cout << "\nLoading model..." << std::endl;
	t2 = clock();
	CascadeRegressor cas_load;
	cas_load.LoadCascadeRegressor(model_name); // !!!This takes a long time!!!
	t3 = clock();
	cout << "load model done" << endl;
	cout << "\ntime to load model: ";
	print_time(t3 - t2);

	std::cout << "\nLoading face detector..." << std::endl;
	std::string fn_haar = "data/haarcascade_frontalface_alt2.xml";
	cv::CascadeClassifier detector_opencv;
	bool yes = detector_opencv.load(fn_haar);
	std::cout << "face detector loaded : " << yes << std::endl;

	frontal_face_detector detector_dlib = get_frontal_face_detector();

	std::vector<cv::Mat_<uchar> > images;
	std::vector<cv::Mat_<cv::Vec3b> > images_color;
	std::vector<cv::Mat_<double> > ground_truth_shapes;
	std::vector<BoundingBox> bboxes;

	t1 = clock();
	cout << "\ntime to start: ";
	print_time(t1 - t0);
	cout << endl;

	std::cout << "\nLoading test dataset..." << std::endl;
	if (images_has_ground_truth) {
		LoadImages(images, ground_truth_shapes, bboxes, image_path_prefixes, image_lists, detector_opencv, detector_dlib, selection);
		double error = 0.0;
		for (int i = 0; i < images.size(); i++) {
			cv::Mat_<double> current_shape = ReProjection(cas_load.params_.mean_shape_, bboxes[i]);
			cv::Mat_<double> res = cas_load.Predict(images[i], current_shape, bboxes[i]);//, ground_truth_shapes[i]);
			double e = CalculateError(ground_truth_shapes[i], res);
			std::cout << "error:" << e << std::endl;
			error += e;
			DrawPredictedImage(images[i], res);
		}
		std::cout << "error: " << error << ", mean error: " << error / images.size() << std::endl;
	}
	else {
		LoadImages(images, images_color, bboxes, image_path_prefixes, image_lists, detector_opencv, detector_dlib, selection); //loads images and detects faces
		//LoadImages(images, images_color, bboxes, image_path_prefixes, image_lists, detector_dlib); //loads images and detects faces
		unsigned long int t0, t1;

		cout << "number of images: " << images.size() << endl;
		for (int i = 0; i < images.size(); i++) {
			t0 = clock();
			cv::Mat_<double> current_shape = ReProjection(cas_load.params_.mean_shape_, bboxes[i]);
			cv::Mat_<double> res = cas_load.Predict(images[i], current_shape, bboxes[i]);//, ground_truth_shapes[i]); //predicts landmarks
			t1 = clock();
			std::cout << "time extracting landmarks: " << t1 - t0 << "ms" << std::endl;

			DrawPredictedImage(images_color[i], bboxes[i], res);
			//DrawPredictedImage(images[i], bboxes[i]);
		}
	}
}

void Test(const char* config_file_path, string selection){
	
	unsigned long int t0, t1, t2, t3;
	t0 = clock();

	cout << "parsing config_file: " << config_file_path << endl;

    ifstream fin;
    fin.open(config_file_path, ifstream::in);
	std::string model_name;
    fin >> model_name;
    cout << "model name is: " << model_name << endl;
	bool images_has_ground_truth = false;
	fin >> images_has_ground_truth;
	if (images_has_ground_truth) {
		cout << "the image lists must have ground_truth_shapes!\n" << endl;
	}
	else{
		cout << "the image lists does not have ground_truth_shapes!!!\n" << endl;
	}

	int path_num;
    fin >> path_num;
    cout << "reading testing images paths: " << endl;
	std::vector<std::string> image_path_prefixes;
    std::vector<std::string> image_lists;
    for (int i = 0; i < path_num; i++) {
        string s;
        fin >> s;
        cout << s << endl;
        image_path_prefixes.push_back(s);
        fin >> s;
        cout << s << endl;
        image_lists.push_back(s);
    }

	cout << "parsing config file done\n" << endl;
	t2 = clock();
	CascadeRegressor cas_load;
	cas_load.LoadCascadeRegressor(model_name); // !!!This takes a long time!!!
	t3 = clock();
	cout << "load model done\n" << endl;
	print_time(t3 - t2);
	
	std::cout << "\nLoading face detector..." << std::endl;
	std::string fn_haar = "data/haarcascade_frontalface_alt2.xml";
	cv::CascadeClassifier detector_opencv;
	bool yes = detector_opencv.load(fn_haar);
	std::cout << "face detector loaded : " << yes << std::endl;

	frontal_face_detector detector_dlib = get_frontal_face_detector();

	std::vector<cv::Mat_<uchar> > images;
	std::vector<cv::Mat_<cv::Vec3b> > images_color;
	std::vector<cv::Mat_<double> > ground_truth_shapes;
	std::vector<BoundingBox> bboxes;

	t1 = clock();
	cout << "\ntime to start: ";
	print_time(t1 - t0);
	cout << endl;

	std::cout << "\nLoading test dataset..." << std::endl;
	if (images_has_ground_truth) {
		LoadImages(images, ground_truth_shapes, bboxes, image_path_prefixes, image_lists, detector_opencv, detector_dlib, selection);
		double error = 0.0;
		for (int i = 0; i < images.size(); i++){
			cv::Mat_<double> current_shape = ReProjection(cas_load.params_.mean_shape_, bboxes[i]);
	        cv::Mat_<double> res = cas_load.Predict(images[i], current_shape, bboxes[i]);//, ground_truth_shapes[i]);
			double e = CalculateError(ground_truth_shapes[i], res);
			std::cout << "error:" << e << std::endl;
			error += e;
	        DrawPredictedImage(images[i], res);
		}
		std::cout << "error: " << error << ", mean error: " << error/images.size() << std::endl;
	}
	else{
		LoadImages(images, images_color, bboxes, image_path_prefixes, image_lists, detector_opencv, detector_dlib, selection); //loads images and detects faces
		unsigned long int t0, t1;
		for (int i = 0; i < images.size(); i++){
			t0 = clock();
			cv::Mat_<double> current_shape = ReProjection(cas_load.params_.mean_shape_, bboxes[i]);
	        cv::Mat_<double> res = cas_load.Predict(images[i], current_shape, bboxes[i]);//, ground_truth_shapes[i]); //predicts landmarks
			t1 = clock();
			std::cout << "time extracting landmarks: " << t1 - t0 << "ms" << std::endl;

			DrawPredictedImage(images_color[i], bboxes[i], res);
		}
	}

}

void Train(const char* config_file_path, string selection){

	unsigned long int t0, t1;
	t0 = clock();
	
	print_local_time();
	cout << "parsing config_file: " << config_file_path << endl;

    ifstream fin;
    fin.open(config_file_path, ifstream::in);
	std::string model_name;
    fin >> model_name;
    cout << "\nmodel name is: " << model_name << endl;
    Parameters params = Parameters();
    fin >> params.local_features_num_
        >> params.landmarks_num_per_face_
        >> params.regressor_stages_
        >> params.tree_depth_
        >> params.trees_num_per_forest_
        >> params.initial_guess_
		>> params.overlap_;
	params.model_name_ = model_name;

    std::vector<double> local_radius_by_stage;
    local_radius_by_stage.resize(params.regressor_stages_);
    for (int i = 0; i < params.regressor_stages_; i++){
            fin >> local_radius_by_stage[i];
    }
    params.local_radius_by_stage_ = local_radius_by_stage;
    params.output();

    int path_num;
    fin >> path_num;
	print_local_time();
    cout << "\nreading training images paths: " << endl;

	std::vector<std::string> image_path_prefixes;
    std::vector<std::string> image_lists;
    for (int i = 0; i < path_num; i++) {
        string s;
        fin >> s;
        cout << s << endl;
        image_path_prefixes.push_back(s);
        fin >> s;
        cout << s << endl;
        image_lists.push_back(s);
    }

    fin >> path_num;
	print_local_time();
    cout << "\nreading validation images paths: " << endl;
	std::vector<std::string> val_image_path_prefixes;
    std::vector<std::string> val_image_lists;
    for (int i = 0; i < path_num; i++) {
        string s;
        fin >> s;
        cout << s << endl;
        val_image_path_prefixes.push_back(s);
        fin >> s;
        cout << s << endl;
        val_image_lists.push_back(s);
    }

    cout << "parsing config file done\n" << endl;

	print_local_time();
	std::cout << "\nLoading face detectors..." << std::endl;
	std::string fn_haar = "data/haarcascade_frontalface_alt2.xml";
	cv::CascadeClassifier detector_opencv;
	bool yes = detector_opencv.load(fn_haar);

	frontal_face_detector detector_dlib = get_frontal_face_detector();
	std::cout << "face detectors loaded : " << yes << std::endl;

	std::vector<cv::Mat_<uchar> > images;
	std::vector<cv::Mat_<double> > ground_truth_shapes;
	std::vector<BoundingBox> bboxes;

	std::vector<cv::Mat_<uchar> > val_images;
	std::vector<cv::Mat_<double> > val_ground_truth_shapes;
	std::vector<BoundingBox> val_bboxes;
	print_local_time();
	std::cout << "\nLoading training dataset..." << std::endl;
	LoadImages(images, ground_truth_shapes, bboxes, image_path_prefixes, image_lists, detector_opencv, detector_dlib, selection); //loads images and detects faces
	if (val_image_lists.size() > 0) {
		std::cout << "\nLoading validation dataset..." << std::endl;
		LoadImages(val_images, val_ground_truth_shapes, val_bboxes, val_image_path_prefixes, val_image_lists, detector_opencv, detector_dlib, selection);
	}
	 else{
	 	std::cout << "your validation dataset is 0" << std::endl;
	 }

	params.mean_shape_ = GetMeanShape(ground_truth_shapes, bboxes);
	CascadeRegressor cas_reg;
	cas_reg.val_bboxes_ = val_bboxes;
    cas_reg.val_images_ = val_images;
    cas_reg.val_ground_truth_shapes_ = val_ground_truth_shapes;

	std::vector<std::vector<double>> errors;
	print_local_time();
	std::cout << "\nStarting training..." << std::endl;
	cas_reg.Train(images, ground_truth_shapes, bboxes, params, errors);
	std::cout << "finish training" << endl;
	

	print_local_time(); cout << endl << "\nStarting to save the model..." << std::endl;
	std::cout << "model name: " << model_name << std::endl;
	cas_reg.SaveCascadeRegressor(model_name);
	std::cout << "model saved successfully" << std::endl;

	cout << endl << "\nStarting to save conditions..." << std::endl;
	std::ofstream fout;
	fout.open((model_name + "_conditions.txt").c_str(), std::fstream::out);
	fout << selection << endl;
	fout.close();
	std::cout << "conditions saved successfully" << std::endl;

	cout << endl << "\nStarting to save errors..." << std::endl;
	fout.open((model_name + "_errors.txt").c_str(), std::fstream::out);
	fout << fixed;
	fout << setprecision(8);
	for (int i = 0; i < errors.size(); i++) {
		std::vector<double> v = errors[i];
		for (int j = 0; j < v.size(); j++) {
			fout << v[j] << " ";
		}
		fout << endl;
	}
	fout.close();
	std::cout << "errors saved successfully" << std::endl;

	t1 = clock();
	cout << "total time: ";
	print_time(t1 - t0);

	cout << endl << "\nStarting to save time..." << std::endl;
	fout.open((model_name + "_results.txt").c_str(), std::fstream::out);
	fout << t1 - t0 << endl;
	fout.close();
	std::cout << "time saved successfully\n" << std::endl;
	print_local_time();
}

void getArgumets(int *argc, char* argv[], const char* arg_path) {
	ifstream fin;
	fin.open(arg_path, ifstream::in);
	fin >> *argc;
	cout << "arguments: " << *argc << endl;
	*argv = new char[*argc];
	string input;
	for (int i = 0; i < *argc; i++) {
		fin >> input;
		argv[i] = new char[input.length()+1];
		strcpy(argv[i], input.c_str());
		cout << "argument[" << i << "]: " << argv[i] << endl;
	}
}

int main(int argc, char* argv[])
{
	//argc = 3;
	//*argv = new char[argc];
	//argv[0] = "freesouls";
	//argv[1] = "train";
	////argv[2] = "example/helen_test_config_images_without_ground_truth.txt"; //to test
	//argv[2] = "example/helen_train_config.txt"; //to train

	getArgumets(&argc, argv, "example/arguments.txt");

	std::cout << "\nuse [./application train train_config_file] to train models" << std::endl;
	std::cout << "    [./application test test_config_file] to test images\n\n" << std::endl;


	if (argc == 4) {
		if (strcmp(argv[1], "train") == 0)
		{
			Train(argv[3], argv[2]);
		}
		if (strcmp(argv[1], "test") == 0)
		{
			TestImage(argv[3], argv[2]);
		}
		if (strcmp(argv[1], "webcam") == 0)
		{
			TestWebcam(argv[2]);
		}
	}
	else {
		std::cout << "\nWRONG!!!" << std::endl;
	}

	system("pause");
	return 0;
}
