#ifndef HEADERS_H
#define HEADERS_H
#define _CRT_SECURE_NO_WARNINGS  //to avoid error C4996 with fscanf and others (unsafe functions)
#include "utils.h"
#include "regressor.h"
#include "randomforest.h"
#include "liblinear/linear.h"
#endif