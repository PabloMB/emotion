#pragma once

#include <iostream>

void print_time(unsigned long int time) {
	int millis = time % 1000; //get milliseconds
	time /= 1000; //change time to seconds
	int seconds = time % 60; //get seconds
	time /= 60; //change time to minutes
	int minutes = time % 60; //get minutes
	time /= 60; //change time to hours
	int hours = time; //get hours

	std::cout << hours << "h ";
	std::cout << minutes << "min ";
	std::cout << seconds << "s ";
	std::cout << millis << "ms ";
	std::cout << std::endl;
}